#!/usr/bin/env groovy
import jenkins.security.s2m.AdminWhitelistRule
import jenkins.model.Jenkins

Jenkins.instance.injector.getInstance(AdminWhitelistRule.class)
    .setMasterKillSwitch(false);
Jenkins.instance.save()