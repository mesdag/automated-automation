#!/usr/bin/env groovy
import com.cloudbees.hudson.plugins.folder.*
import com.cloudbees.hudson.plugins.folder.Folder
import com.cloudbees.hudson.plugins.folder.properties.*
import com.cloudbees.hudson.plugins.folder.properties.FolderCredentialsProvider.FolderCredentialsProperty

import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.CredentialsStore

import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

import jenkins.model.Jenkins
import jenkins.plugins.git.*
import jenkins.branch.BranchSource
import jenkins.branch.DefaultBranchPropertyStrategy
import jenkins.plugins.git.GitSCMSource
import jenkins.plugins.git.traits.BranchDiscoveryTrait
import jenkins.plugins.git.traits.CleanBeforeCheckoutTrait
import jenkins.plugins.git.traits.CleanAfterCheckoutTrait
import jenkins.plugins.git.traits.LocalBranchTrait

import org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject
import org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProjectFactory

import io.jenkins.blueocean.rest.impl.pipeline.credential.BlueOceanDomainSpecification

// parameters
def jobCredentialParameters = [
  description:  'jenkins-generated-ssh-key',
  id:           'jenkins-generated-ssh-key',
  username:     '',
  private_key:   '''
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAxL/aaRs/lS+sphW63/kbpu3jILsSlvnaaEEAnb11LvpMtDDh
ntrkHq61yb+TeLhnbKQ8TFdEmC36OqG7DdIW+mb3AHA8dQ+2sLj7/lkeojvarBCg
e4qf+C5Qex+tVD/8E62SBnjTq7ZeNG84E89dNND/e28nuHgpGTCI6901duF9P1uW
JOqNdTijUCUkggyzAXf6N4bxNf9RTujmFFTBAUYIxUn2MxbjniaRPnTOgbI4N11c
c7ZB92k9BRokn0hk3z3nT+35c5m7QkhqdTXs46OtCwGd2xc6UFf4stI44aXYSzrv
vDi+1SQGLr+Ug3YNAvMBgzl82FnMfIWAotbsv33IZsbGrOetlLXRwMJn6n3+Qk5x
KgVgnzyMP375kJnj9BMzwWtvJtlNcC6KFSI/wqvyPwYlJ4y4lDr+ZmoryeakkyI+
2/gLzO4aP6u6GotbKlXANGcfwNdgxPyJaUXo0lbOpjG5fLV7laWvFaVvYxYkEAJu
ptn8ElNK79Nypn2O6qUaDQX44mb4iqYyrHNk7z4zRu/J8QznAdLNotRDlRjQaXZi
UWS4bsu5baC9Sh4uqWvs3o1MsAvPPrqEj57T8Mqd0airkLl+9mEtp0JZ53QZLo/o
F7QdcojPvjwpBSamb4Ax3YSG0W06LJACe4hyuQDKApOZQffj4QvGksLrPz0CAwEA
AQKCAgAcZmUsjtZavor5XmGfOuIW8ne6ncZNLLuuGNPx8QsdQNwZloS63XXYR/fN
q22qG1QyhnJ9wosQPHLGS9ooYI8YjgxRb+IKM753O/yD9nFJkcI63BuOObBlrqnH
XAc1Bhdo78EIViCXxLdMclFx6pjVquV199GP+yhTjoH1KfUxt9LQdnPx2DmbFhXc
17+xVgvwxlTD9yKSXMxry4qKnMFPYcCaWdbEASXSClck4M9+QS+ELmHOAhz7jaFV
PzTSGS0WXGUrUHqViTdSlgln6eTL5zBvSkhy+6NbPZeUmGvFhp72HQfF75r8QTrz
PIw/fabbKiCpxH6U7EFsKAkteECsErusYXrU+H2cv5DNfQoqnYdTvMasg2LDT3vC
lo7SpjAI57g4ENiN1Xxxar8NM8jwjZBxHQMBsHNZHW0TlL24nXssRLepXbDn1URI
f5S88IHnrRKWlEvzr/Au7vWPzDNwzZEgIsjoonqEjXBWHrBS9Wt3bHUbxlQjcFRx
jFJPYDyIep6oCXXptfiw8lEd+0ufPULMqmlP7HaLh0nlciob3vxugInZtuH65W+P
6wzcPfabnFt7AA9U0P3+aZ+cycfZuVjifSkG8gPaMLVGCd/EmpjpkZ7UrLANHukb
bUSPwTVlyADOwcX7dYEri165oEVGKgJjrRnjIm7tQBQPp/KOMQKCAQEA46p/y/kE
QPOT3RLpFLSZNn1nBuYoqg1iPohNg56z4gdtdNBXiVhYydl0lhV++YBwqBCyXoda
BBJKZsQpJNkdfT68YQcQ3niCrTu7Rws4uWbIv10qJnnaboTPwDOp61xf/8xxw8TQ
pfaelr/IeGeLFwCvZSOxeSFb3L80lM4Xt343sUMqaKOfqu8/3fMjmHCYytiYlkBF
XDDrseszqJKMVQ0LhuYS3CESZRoLmnwvsea64jJqE1kqag2xAaeVqq83mG5o/cBm
Kv6M1jxtnL/Ck9CMEaiSyO1P8Vsw+BYS1jdsx1J8xunMg77DV3zyHFy8S+ky3WXJ
Y3aTh7/zALhpwwKCAQEA3TxX2SkPST20SQ/jwItiY+tV6yUA3jaWqrEU4eENudhv
5NNYUZCd1gV+fGg/fHigiJATmOROQIe4iv6RpyQzFyMcytqJruRuyBB6i/FUonda
XNjttLoIufA6+ATQTO4qOKXTVgFaPmma8VwZkb2msqbDqS91Oy/2UKnF1P/RDyIb
bpzm8PQv5ylA9XBYWaqN2ShnMypaBnuiXVrkLqBPM+Wj8aQEfohf/SAMw8VKMOKu
DUVZ3Eh4hoKt4Z3pkioKdK93qIzhu8fZPJZVRcGWJRYwHh2Ti3rGgMWQcvHLuyHb
7rVpjARcK+15HkPOE63lxygZcMYHWLphyNWYnToi/wKCAQEAyz6sJmFhncaWK5e1
Q0nTOeUL9AnHJcEbAu5WHPmyZ0fwPIe+jhuz8EdUoKQCHiW+7Rsy0dcN0nKtvz7B
6GZ3wBcgv5I6ShHVv2eC0nK/Ki0LbGN0vLKuAVpJybGVsUtfXJKcwYPULUWU4MnH
4020y01OSAFsb9ILG3ALNgki/SoN+A8ZLh9c7JH4N2HXVkC29ugUmJY6FYqK7+Qk
0wo5g7hIoR6GmcRgkqeE0l6pia2NC2CseCdzCQi+C9cbcyxHeqqeymJYE2v6OUvr
IzPCNonIcf9HQ18wriyV8/RozpreXrdSM5YHgVntNA6UaiOT6kT+ps420rfKDzea
bfs0VwKCAQAaH/UTvMiEUpgGhKzVYOBGglTMBruaggG6uFyJ6+SODKPx9o8YWqZ4
zn9IO5s7nJcAaVsXE+a1shE1wKf8f8bUtZ3Bypq1n14BnAiMm/vtj4CJNEilrc43
/slH3nGmk/1CYTqajmBUvG3CkKioUCfuMskqImOKkLhagCHPbEIIsnNg1YBj9cHu
Xy2/jfuw4vYLLb06CodE+raKpaAKbX3M2BJLB55MPthu8KjxOUvwh+QbxxO8coZh
A+96lAYv72z6qHnN2o0ReqEJiYsaSKjtSjh2ZfG1Z9MnA61yFWGEgJQTFTMYiK9F
tK43x7P/GOroYLDYY1ocvS2s5KQ39NSLAoIBAQDchg91ki3mm2FSYN9aOYtkXhoU
UP3YwUH7ufmwFwsWx5oLboypROKjUL/s/wP6WXcXp6RuOVw/9M7GvFz3zyT47G3c
4IYlVCAuGokBdj9KmoRkmwvFGtiWGsXSibBkZRsYIhK1btKbz8POR1qWW09ONbc7
jFNUBwCJ0o+Zwi+QOIVU+NNvuhQSzoHsBnDppHLJopNPsUpK+XqdKexYR6sk9+B3
yucR0NlCn46wgPqqlpGpxXNjk1/FPBDnQQecWbObEYdZ+X1SBfZ662+Nk+5TSrW/
2T/FBG9Zceh19m3LXjg2wow6F4wFCoeo6sR+u64pW2P1MwJVDRNVeX+HsSye
-----END RSA PRIVATE KEY-----
''',
  password:    '',
  credentialDomain:     'blueocean-folder-credential-domain',
  credentialDomainDesc: 'BlueOcean Folder Credentials'
]

// Define parameters for all the core jobs we want to create
def jobParameters = [
  jobname:      'awx-jenkins',
  displayName:  '',
  description:  '',
  jenkinsfile:  'Jenkinsfile-2',
  repository:   'git@gitlab.com:allyourcode/test.git',
  credentialId: jobCredentialParameters.id
]

// get Jenkins instance
Jenkins jenkins = Jenkins.getInstance()

// Get this once to check for existing jobs
jobs = jenkins.getAllItems()

println '----> checking for existence of job ' + jobParameters.jobname

def shouldCreate = true
// check to see if we've created this job already
jobs.each { j ->
  if (j instanceof org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject &&
    j.fullName.contains(jobParameters.jobname)) {
    println '----> Already have a job for ' + j.fullName + ' of type: ' + j.getClass()
    shouldCreate = false
  }
}

if (shouldCreate) {

  println '----> creating ' + jobParameters.jobname

  // start by creating the toplevel folder
  def project = jenkins.createProject(WorkflowMultiBranchProject, jobParameters.jobname)

  project.description = jobParameters.description
  project.displayName = jobParameters.displayName

  // configure git scm
  def sources = project.getSourcesList()
  def gitSCMSource = new GitSCMSource(null, jobParameters.repository, jobParameters.credentialId, "*", "", false)
  DefaultBranchPropertyStrategy strategy = new DefaultBranchPropertyStrategy(null)

  def traits = new ArrayList()

  def branchTrait = new BranchDiscoveryTrait()
  traits.add(branchTrait)

  def cleanBeforeTrait = new CleanBeforeCheckoutTrait()
  traits.add(cleanBeforeTrait)

  def cleanAfterTrait = new CleanAfterCheckoutTrait()
  traits.add(cleanAfterTrait)

  def localBranchTrait = new LocalBranchTrait()
  traits.add(localBranchTrait)

  gitSCMSource.setTraits(traits)

  def source = new BranchSource(gitSCMSource, strategy)
  sources.add(source)

  // define credentials
  def credentials
  jobCredentialParameters.private_key = jobCredentialParameters.private_key.trim()
  if (jobCredentialParameters.private_key == "" ) {
    credentials = new UsernamePasswordCredentialsImpl(
      CredentialsScope.GLOBAL,
      jobCredentialParameters.id,
      jobCredentialParameters.description,
      jobCredentialParameters.username,
      jobCredentialParameters.password
    )
  } else {
    def key_source
    if (jobCredentialParameters.private_key.startsWith('-----BEGIN')) {
      key_source = new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(jobCredentialParameters.private_key)
    } else {
      key_source = new BasicSSHUserPrivateKey.FileOnMasterPrivateKeySource(jobCredentialParameters.private_key)
    }
    credentials = new BasicSSHUserPrivateKey(
      CredentialsScope.USER,
      jobCredentialParameters.id,
      jobCredentialParameters.username,
      key_source,
      jobCredentialParameters.password,
      jobCredentialParameters.description
    )
  }

  // get credentials store
  AbstractFolder<?> folderAbs = AbstractFolder.class.cast(project)
  FolderCredentialsProperty property = folderAbs.getProperties().get(FolderCredentialsProperty)
  if(!property) {
    property = new FolderCredentialsProperty()
    folderAbs.addProperty(property)
  }
  def store = property.getStore()

  // // add credentials folder
  // project.addItem("folder")

  // set credentials domain
  Domain domain = new Domain(jobCredentialParameters.credentialDomain, jobCredentialParameters.credentialDomainDesc, Collections.<DomainSpecification>singletonList(new BlueOceanDomainSpecification()))
  store.addDomain(domain);

  // Create the credentials in the Jenkins instance
  println '----> adding credentials ' + jobCredentialParameters.description
  store.addCredentials(domain, credentials)

  // Save changes
  jenkins.save()

  // Trigger multi-pipeline scan
  project.scheduleBuild()

  println '----> configured job ' + jobParameters.jobname
}