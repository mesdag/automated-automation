#!/usr/bin/env groovy
import jenkins.model.JenkinsLocationConfiguration

jlc = JenkinsLocationConfiguration.get()
jlc.setUrl("http://{{ jenkins_hostname }}:{{ jenkins_http_port }}{{ jenkins_url_prefix }}")
jlc.save()