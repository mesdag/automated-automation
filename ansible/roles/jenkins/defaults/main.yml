---
# Optional method of pinning a specific version of Jenkins and/or overriding the
# default Jenkins packaging URL.
# jenkins_version: "1.644"
# jenkins_pkg_url: "https://www.example.com"

# Change this to `latest` to update Jenkins if a newer version is available.
jenkins_package_state: present

jenkins_connection_delay: 5
jenkins_connection_retries: 60
jenkins_home: /var/lib/jenkins
jenkins_hostname: localhost
jenkins_http_port: 8080
jenkins_jar_location: /opt/jenkins-cli.jar
jenkins_url_prefix: ""
jenkins_java_options: "-Djenkins.install.runSetupWizard=false"

jenkins_plugins:
  - "ace-editor"
  - "ansicolor"
  - "antisamy-markup-formatter"       #OWASP Markup Formatter
  - "apache-httpcomponents-client-4-api"
  - "authentication-tokens"
  - "badge"
  - "basic-branch-build-strategies"
  - "bouncycastle-api"
  - "branch-api"
  - "blueocean-autofavorite"
  - "blueocean-display-url"
  - "blueocean-bitbucket-pipeline"
  - "blueocean-commons"
  - "blueocean-config"
  - "blueocean-core-js"
  - "blueocean-dashboard"
  - "blueocean-events"
  - "blueocean-git-pipeline"
  - "blueocean-github-pipeline"
  - "blueocean-i18n"
  - "blueocean-jira"
  - "blueocean-jwt"
  - "blueocean-personalization"
  - "blueocean-pipeline-api-impl"
  - "blueocean-pipeline-editor"
  - "blueocean-pipeline-scm-api"
  - "blueocean-rest-impl"
  - "blueocean-rest"
  - "blueocean-web"
  - "blueocean"                       #Blue Ocean
  - "cloud-stats"                     #Cloud Statistics
  - "cloudbees-bitbucket-branch-source"
  - "cloudbees-folder"
  - "cobertura"                       #Cobertura
  - "command-launcher"                #Command Agent Launcher
  - "config-file-provider"
  - "credentials"                     #Credentials
  - "credentials-binding"
  - "dashboard-view"                  #Dashboard View
  - "dependency-check-jenkins-plugin" #OWASP Dependency-Check
  - "display-url-api"
  - "docker-plugin"                   #Docker
  - "docker-commons"                  #Docker Commons
  - "docker-workflow"
  - "durable-task"                    #Durable Task
  - "embeddable-build-status"         #embeddable-build-status
  - "favorite"
  - "git"
  - "git-client"
  - "git-server"
  - "github"
  - "github-api"
  - "github-branch-source"
  - "github-oauth"
  - "gitlab-plugin"                   #GitLab
  - "gravatar"                        #Gravatar
  - "groovy"                          #Groovy
  - "groovy-postbuild"
  - "groovy-label-assignment"         #Groovy Label Assignment plugin
  - "handlebars"
  - "handy-uri-templates-2-api"
  - "htmlpublisher"                   #HTML Publisher
  - "icon-shim"                       #Icon Shim
  - "jackson2-api"
  - "javadoc"                         #Javadoc
  - "jdk-tool"                        #JDK Tool
  - "jenkins-design-language"
  - "jira"
  - "job-dsl"                         #Job DSL
  - "job-restrictions"
  - "jquery-detached"
  - "jsch"
  - "junit"
  - "mailer"
  - "mask-passwords"                  #Mask Passwords
  - "matrix-auth"
  - "matrix-project"
  - "maven-plugin"
  - "mercurial"
  - "momentjs"
  - "pipeline-build-step"
  - "pipeline-graph-analysis"
  - "pipeline-input-step"
  - "pipeline-milestone-step"
  - "pipeline-model-api"
  - "pipeline-model-declarative-agent"
  - "pipeline-model-definition"
  - "pipeline-model-extensions"
  - "pipeline-multibranch-defaults"
  - "pipeline-rest-api"
  - "pipeline-stage-step"
  - "pipeline-stage-tags-metadata"
  - "pipeline-stage-view"
  - "plain-credentials"
  - "pubsub-light"
  - "rich-text-publisher-plugin"      #Rich Text Publisher
  - "scm-api"
  - "scm-filter-branch-pr"            #SCM Filter Branch PR
  - "script-security"
  - "slack"                           #Slack Notifications
  - "sonar"                           #SonarQube Scanner
  - "sse-gateway"
  - "ssh-credentials"                 #SSH Credentials
  - "ssh-slaves"                      #SSH Slaves
  - "structs"
  - "token-macro"
  - "variant"
  - "view-job-filters"                #View Job Filters
  - "windows-slaves"
  - "workflow-aggregator"             #Pipeline
  - "workflow-api"
  - "workflow-basic-steps"
  - "workflow-cps"
  - "workflow-cps-global-lib"
  - "workflow-durable-task-step"
  - "workflow-job"
  - "workflow-multibranch"
  - "workflow-scm-step"
  - "workflow-step-api"
  - "workflow-support"
  - "zap"                             #Official OWASP ZAP
  - "yet-another-docker-plugin"       #Yet Another Docker
jenkins_plugins_state: present
jenkins_plugin_updates_expiration: 86400
jenkins_plugin_timeout: 60 #30
jenkins_plugins_install_dependencies: yes

jenkins_admin_username: admin
jenkins_admin_password: password
jenkins_admin_password_file: ""
jenkins_admin_token: ""
jenkins_admin_token_file: ""

jenkins_process_user: jenkins
jenkins_process_group: "{{ jenkins_process_user }}"

jenkins_init_changes:
  - option: "JENKINS_ARGS"
    value: "--prefix={{ jenkins_url_prefix }}"
  - option: "{{ jenkins_java_options_env_var }}"
    value: "{{ jenkins_java_options }}"

jenkins_slack_workspace_subdomain: mycompany
jenkins_slack_token: ""
jenkins_slack_channel: "#jenkins"
