---
- name: Ensure dependencies are installed.
  package:
    name:
      - curl
      - libselinux-python
      - initscripts
    state: present

- name: Ensure Jenkins repo is installed.
  get_url:
    url: "{{ jenkins_repo_url }}"
    dest: /etc/yum.repos.d/jenkins.repo
  when: jenkins_repo_url != ''

- name: Add Jenkins repo GPG key.
  rpm_key:
    state: present
    key: "{{ jenkins_repo_key_url }}"

- name: Download specific Jenkins version.
  get_url:
    url: "{{ jenkins_pkg_url }}/jenkins-{{ jenkins_version }}-1.1.noarch.rpm"
    dest: "/tmp/jenkins-{{ jenkins_version }}-1.1.noarch.rpm"
  when: jenkins_version is defined

- name: Check if we downloaded a specific version of Jenkins.
  stat:
    path: "/tmp/jenkins-{{ jenkins_version }}-1.1.noarch.rpm"
  register: specific_version
  when: jenkins_version is defined

- name: Install our specific version of Jenkins.
  package:
    name: "/tmp/jenkins-{{ jenkins_version }}-1.1.noarch.rpm"
    state: present
  when: jenkins_version is defined and specific_version.stat.exists
  notify: configure jenkins

- name: Ensure Jenkins is installed.
  package:
    name: jenkins
    state: "{{ jenkins_package_state }}"
  notify: configure jenkins

- name: Check if firewall is active.
  command: systemctl is-active firewalld
  register: result
  ignore_errors: yes
  changed_when: false
  tags:
    - skip_ansible_lint  # ANSIBLE0006

- name: Add firewall rule.
  firewalld:
    port: "{{ jenkins_http_port }}/tcp"
    permanent: true
    state: enabled
    immediate: yes
  when: result.stdout == "active"
