# Automated Automation

![](https://i.imgur.com/2QGUob9.png)
![](https://i.imgur.com/10mqaG6.png)
![](https://i.imgur.com/Ua8xaFh.png)
![](https://i.imgur.com/y2l5siF.png)

Basically a bunch of roles to install AWX & Jenkins that can be run locally with Vagrant.

## Using Vagrant

### Requirements

The following software needs to be installed:  
+ [Vagrant](https://www.vagrantup.com/)  
+ [Ansible](https://www.ansible.com/)  
+ [Virtualbox](https://www.virtualbox.org/)  

For notifications:  
+ [Slack Account](https://www.virtualbox.org/)  

### Optional

Install some extra Vagrant plugins `vagrant plugin install vagrant-protect vagrant-hostmanager vagrant-registration vagrant-cachier`.

### Setup

Please consult the `servers.yml` file for the provisioning options (`centos7`/`rhel7`).
If you want to use RHEL 7 then clone [this packer repository](https://gitlab.com/mesdag/packer/tree/master/rhel) **next** to this one. See the packer repository for instructions how to create the vagrant box. Also edit the `server.yml` file to point to the created vagrant box.

1. Check the `servers.yml` file for the provisioning options and edit the `secrets.yml` with your information and optionally encrypt it with `ansible-vault encrypt secrets.yml`. If you choose to encrypt then set `ask_vault_pass: true` to be prompted for the vault password or set `vault_password_file: password.txt` with the path to the password file in the `servers.yml` file.

2. Bring up all the machines with `vagrant up` or individually with `vagrant up awx` and with `vagrant up jenkins`, provisioning should take **~20 minutes** for all the VM's - Grab yourself a cup of coffee.

3. For AWX login via [http://172.25.0.10](http://172.25.0.10) and with the following credentials:  
   **username:** admin  
   **password:** password
   
   For Jenkins login via [http://172.25.0.11:8080](http://172.25.0.11:8080/blue) and with the following credentials:  
   **username:** admin  
   **password:** password
   
4. Done!

_Note:_ When using a RHEL 7 vagrant box make sure the Vagrant plugin `vagrant-registration` is installed and set the environment variables `RHSM_USERNAME` and  `RHSM_PASSWORD` to register the VMs on creation:

```
$ export RHSM_USERNAME='<your username>'
$ export RHSM_PASSWORD='<your password>'
```

## Usage

This repository will automatically be added to the created Jenkins instance and it will be scanned for a Jenkinsfile. It will then create the pipeline and run it. Finally it will deploy as new job templates in AWX.

Notifications will be send from the Jenkins pipeline, as well as from AWX to the configured Slack channel.

## Todo

+ Create tests for all roles
+ Deploy as job templates to AWX via the API
+ Complete the README files
+ Complete the meta files
