# -*- mode: ruby -*-
# vi: set ft=ruby :

# Temporary fix for: https://github.com/projectatomic/adb-vagrant-registration/issues/126
# Registration is skiped on rhel7.5 because subscription_manager_registered cap no longer works
module SubscriptionManagerMonkeyPatches
  def self.subscription_manager_registered?(machine)
    true if machine.communicate.sudo("/usr/sbin/subscription-manager list --consumed --pool-only | grep -E '^[a-f0-9]{32}$'")
  rescue
    false
  end
end
VagrantPlugins::Registration::Plugin.guest_capability 'redhat', 'subscription_manager_registered?' do
  SubscriptionManagerMonkeyPatches
end
# End fix

# Imports :
# Require YAML module
require 'yaml'

# Variables :
VAGRANT_DIR = File.expand_path(File.dirname(__FILE__))
VAGRANTFILE_API_VERSION = "2"
Vagrant.require_version ">=1.8.4"

# Read YAML file with box details
servers = YAML.load_file('servers.yml')

# Create boxes
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Plugin : vagrant-cachier plugin
  if Vagrant.has_plugin?("vagrant-cachier")
    # https://github.com/fgrehm/vagrant-cachier
    host = RbConfig::CONFIG['host_os']
    if host == /linux/ || host == /darwin/
      config.cache.scope = :machine
        config.cache.synced_folder_opts = {
          type: :nfs, mount_options: ['rw', 'vers=3', 'tcp', 'nolock']
        }
    end
  end

  # Iterate through entries in YAML file
  servers.each do |servers|

    if servers["username"]
      config.ssh.username = servers["username"]
    end

    if servers["password"]
      config.ssh.password = servers["password"]
      config.ssh.insert_key = false
    end

    config.vm.define servers["name"] do |srv|

      if servers["box_url"] != nil
        srv.vm.box_url = servers["box_url"]
      else
        if servers["box_version"] and
            srv.vm.box_version = servers["box_version"].to_s
            srv.vm.box_check_update = false
          end
      end
        srv.vm.box = servers["box"]

      if servers["ip"]
        srv.vm.network "private_network", ip: servers["ip"]
      end

      if servers["ssh_host_port"]
        # https://realguess.net/2015/10/06/overriding-the-default-forwarded-ssh-port-in-vagrant/
        srv.vm.network :forwarded_port, guest: 22, host: servers['ssh_host_port'], id: "ssh"
      end

      if servers["hostname"] != nil
        srv.vm.hostname = servers["hostname"]
      end

      # Plugin : vagrant-protect
      if Vagrant.has_plugin?("vagrant-protect")
        srv.protect.enabled = servers["protected"]
      end

      # Plugin : vagrant-registration
      if Vagrant.has_plugin?('vagrant-registration')
        if ENV['RHSM_USERNAME'] && ENV['RHSM_PASSWORD']
          now = Time.new
          if servers["hostname"] != nil
            registration_name = servers["hostname"]
          else
            registration_name = servers["name"].gsub(/\s+/, '_').gsub!(/[^0-9A-Za-z\.-_]/, '')
          end
          srv.registration.name = registration_name + "-" + now.strftime("%Y%m%d%H%M")
          srv.registration.username = ENV['RHSM_USERNAME']
          srv.registration.password = ENV['RHSM_PASSWORD']
        end
      end

      # Plugin : vagrant-hostmanager
      if Vagrant.has_plugin?("vagrant-hostmanager")
        srv.hostmanager.enabled = true
        srv.hostmanager.manage_host = true
        srv.hostmanager.manage_guest = true
        srv.hostmanager.ignore_private_ip = false
        srv.hostmanager.include_offline = true
        if servers["hostname"] != nil
          hostname = servers["hostname"].gsub(/^(\w+)\..*$/, '\1')
          srv.hostmanager.aliases = %w("#{hostname}.localdomain" "#{hostname}")
        end
      end

      # Set provider settings
      if servers["provider"] != nil
        case servers["provider"]
        when "virtualbox"
          srv.vm.provider :virtualbox do |vb|
            vb.name = "#{servers["name"]}"
            if servers["cpu"] != nil
              vb.cpus = servers["cpu"]
            end
            if servers["ram"] != nil
              vb.memory = servers["ram"]
            end
            vb.gui = servers["gui"]
            vb.customize ["setextradata", "global", "GUI/SuppressMessages", "all"]
          end
        else
          puts "Supported provider ATM is only Virtualbox ... exiting"
        end
      end

      # Ansible provisioning
      if servers["ansible_provision"] != nil && servers["ansible_provision"]

        srv.vm.provision :ansible do |ansible|
          ansible.limit = servers[:name]

          # Use playbook name from servers.yml
          if servers["playbook"] == nil
             ansible.playbook = "./playbooks/default.yml"
          else
             ansible.playbook = "#{servers["playbook"]}"
          end

          # Pass a custom inventory file
          if servers["inventory"] != nil
             ansible.inventory_path = "#{servers["inventory"]}"
          end

          ansible.become = true
          ansible.become_user = "root"
          ansible.compatibility_mode = "2.0"
          ansible.host_key_checking = false

          # Run in verbose mode
          if servers["ansible_verbose"] != nil && servers["ansible_verbose"]
            ansible.verbose = "-v"
          end

          # Set the password for the vault
          if servers["ask_vault_pass"]
            ansible.ask_vault_pass = true
          elsif servers["vault_password_file"] != nil && servers["vault_password_file"]
            ansible.vault_password_file = "#{servers["vault_password_file"]}"
          end

          # # in case we have requirements for ansible role under
          # ansible.galaxy_roles_path = "./site-roles/"
          # # requirements file
          # # ansible.galaxy_role_file = "./requirements.yml"
          # # extra_vars
          # ansible.extra_vars = {
          #   some_var: "some_value",
          #   dict: {
          #     kay1: "val1",
          #     kay2: "val2"
          #   }
          # }

        end
      end
    end
  end
end
