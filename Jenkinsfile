#!/usr/bin/env groovy

node('docker.local.jenkins.slave') {

  // currentBuild.displayName = "fooName"
  // currentBuild.description = "fooDescription"

  properties([
      pipelineTriggers(createPipelineTriggers()),
      // disableConcurrentBuilds(),
      buildDiscarder(logRotator(numToKeepStr: '10'))
  ])

  environment {
    slack_channel = '#awx-jenkins'
    roles = ''
  }

  catchError {

    slackNotify()

    stage('Prepare') {
      echo 'Preparing Docker container..'
      sh 'sudo yum install -y ansible ansible-lint yamllint'
      echo 'Done!'
    }

    stage('Checkout') {
      checkout scm
      env.roles = sh (script: 'ls -1 ./ansible/roles | tr "\n" " "', returnStdout: true).trim()
      echo 'Done!'
    }

    stage('Check') {
      echo 'Starting syntax check..'
      for (String role : env.roles.split(" ")){
        echo " =========== ${role} ==========="
        sh "yamllint -d \"{extends: default, rules: {line-length: {max: 120, level: warning}, truthy: disable, comments: disable, comments-indentation: disable, document-start: {ignore: meta}}}\" ./ansible/roles/${role}"
        sh "ansible-lint -v ./ansible/roles/${role}"
        sh "export ANSIBLE_ROLES_PATH=./ansible;ansible-playbook -i ./ansible/roles/${role}/tests/inventory ./ansible/roles/${role}/tests/main.yml --syntax-check"
      }
      echo 'Done!'
    }

    // stage('Build') {
    //   echo 'Starting build..'
    //   sh 'mvn clean install -DskipTests'
    //   archiveArtifacts '**/target/*.*ar'
    // }

    stage('Test') {
      echo 'Starting test..'
      for (String role : env.roles.split(" ")){
        echo " =========== ${role} ==========="
        def script = "export ANSIBLE_ROLES_PATH=./ansible; ansible-playbook -i ./ansible/roles/${role}/tests/inventory ./ansible/roles/${role}/tests/main.yml --connection=local --become --skip-tags skip_test"
        sh script
        def result = sh (script: script, returnStdout: true).trim()
        if (result ==~ /.*changed=0.*failed=0.*/) {
          echo 'Idempotence test: pass'
        } else {
          // error('Idempotence test: fail')
        }
        echo result
      }
      echo 'Done!'
    }

    // parallel(
    //   unitTest: {
    //     stage('Unit Test') {
    //       sh 'mvn test'
    //     }
    //   },
    //   integrationTest: {
    //     stage('Integration Test') {
    //       if (isNightly()) {
    //         sh 'mvn verify -DskipUnitTests -Parq-wildfly-swarm '
    //       }
    //     }
    //   }
    // )

    stage('Deploy') {
      when (env.BRANCH_NAME == 'development') {
        echo 'Merging brach development to main..'

        echo 'Bumping version number.'

        echo 'Tagging new release..'

        echo 'Done!'
      }
    }

  }

  // Archive Unit and integration test results, if any
  // junit allowEmptyResults: true,
  //     testResults: '**/target/surefire-reports/TEST-*.xml, **/target/failsafe-reports/*.xml'

  slackNotify(currentBuild.result)
}

def slackNotify(String buildStatus = 'STARTED') {
  // Build status of null means successful.
  buildStatus = buildStatus ?: 'SUCCESS'
  // Replace encoded slashes.
  def decodedJobName = env.JOB_NAME.replaceAll("%2F", "/")
  // Set colors
  def color
  if (buildStatus == 'STARTED') {
    color = '#D4DADF'
  } else if (buildStatus == 'SUCCESS') {
    color = '#BDFFC3'
  } else if (buildStatus == 'UNSTABLE') {
    color = '#FFFE89'
  } else {
    color = '#FF9FA1'
  }
  // Format the message
  def msg = "${buildStatus}: `${decodedJobName}` #${env.BUILD_NUMBER}: ${env.BUILD_URL}"
  // Send the message
  slackSend(channel: env.slack_channel, color: color, message: msg)
}

def createPipelineTriggers() {
  if (env.BRANCH_NAME == 'master') {
    // Run a nightly only for master
    return [pollSCM('H H(0-3) * * 1-5')]
  }
  // Run every 15 minutes by default
  return [pollSCM('H/15 * * * *')]
}
